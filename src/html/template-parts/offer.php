<div class="offer">

    <div class="offer__back">
        <div class="offer__back-txt">
            <div class="offer__title"> Осенний листопад цен! </div>
            <p> Пудровые брови и Микроблейдинг, </p> <p> Стрелки или губы с растушевкой </p>
            <a href="temp.html">Подробнее</a>
        </div>
    </div>

    <div class="offer__front">
        <div class="offer__img">
            <div class="diamond">
                <div class="background"></div>
                <div class="icon"><i class="fa fa-paint-brush"></i></div>
            </div>
        </div>
        <div class="offer__txt">
            Акция
        </div>
    </div>

</div>