<section class="txt bb">
    <div class="wrapper">
        <div class="line">
            <div class="col-6 col-s-12">
               <h2>Подарочный сертификат на татуаж</h2>
               <p>
                   Думаете что подарить маме, сестре, дочери, жене, подруге на Новый Год?
                   Подарите качественный татуаж. В продаже подарочные сертификаты.
                   Срок действия сертификата - 3 месяца (январь, февраль, март 2018 года) Дарите красоту!
               </p>
               <img class="ava-news" src="img/ava.jpg" alt="Марина Обухова">
            </div>
            <div class="col-6 col-s-12">
                <div class="img-wrap">
                    <img src="https://tatuaj-moskva.ru/wp-content/uploads/2017/12/podarochnyj-sertifikat.jpg" alt="Подарочный сертификат на татуаж на Москве">
                </div>
            </div>
        </div>
    </div>
</section>