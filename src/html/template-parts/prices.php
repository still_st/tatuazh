<div class="prices" id="prices" itemscope="" itemtype="http://schema.org/Table">
    <div class="wrapper">
        <table>
            <thead>
                <tr>
                    <th>Услуга</th>
                    <th>Стоимость</th>
                </tr>
            </thead>
            <tbody>

                <tr class="prices__main">
                    <td><a href="https://tatuaj-omsk.ru/brovi/">Брови</a>:</td>
                    <td></td>
                </tr>

                <tr>
                    <td class="pl-30">
                        <a class="prices__link" href="https://tatuaj-omsk.ru/mikrobleiding/">
                            Комбинированная техника волоски с растушевкой
                        </a>
                    </td>

                    <td class="prices__del">
                        <del>5000 руб.</del>
                        4000 руб.
                        <span class="sale">АКЦИЯ</span>
                     </td>
                </tr>

                <tr>
                    <td class="pl-30">
                        <a href="https://tatuaj-omsk.ru/voloski-rastushevka/">
                            Комбинированная техника волоски с растушевкой
                        </a>
                    </td>
                    <td>
                        4500                        руб.
                                            </td>
                </tr>

                <tr>
                    <td class="pl-30">
                        <a href="https://tatuaj-omsk.ru/pudrovye/">
                            Пудровые брови
                        </a>
                    </td>
                    <td>
                        4000                        руб.
                                                        <span class="sale">АКЦИЯ</span>
                                            </td>
                </tr>

                <tr>
                    <td class="pl-30">
                        <a href="https://tatuaj-omsk.ru/tenevaya-tehnika/">
                            Теневая техника
                        </a>
                    </td>
                    <td>
                        4000                        руб.
                                            </td>
                </tr>

                <tr>
                    <td class="pl-30">
                        <a href="https://tatuaj-omsk.ru/gratazh/">
                            Гратаж
                        </a>
                    </td>
                    <td>
                        4000                        руб.
                                            </td>
                </tr>

                <tr>
                    <td class="pl-30">
                        <a href="https://tatuaj-omsk.ru/ombre/">
                            Омбре
                        </a>
                    </td>
                    <td>
                        4000                        руб.
                                            </td>
                </tr>

                <tr>
                    <td class="pl-30">
                        <a href="https://tatuaj-omsk.ru/biotatuazh/">
                            Биотатуаж бровей (окрашивание хной + коррекция бровей)
                        </a>
                    </td>
                    <td>
                        450                        руб.
                                            </td>
                </tr>



                <tr class="prices__main">
                    <td>
                        <a href="https://tatuaj-omsk.ru/glaza/">
                            Глаза (Веки)</a>:
                    </td>
                    <td>

                    </td>
                </tr>


                <tr>
                    <td class="pl-30">
                        <a href="https://tatuaj-omsk.ru/mezhresnichka/">
                            Межресничка
                        </a>
                    </td>
                    <td>
                        2500                        руб.
                                                        <span class="sale">АКЦИЯ</span>
                                            </td>
                </tr>
                <tr>
                    <td class="pl-30">
                        <a href="https://tatuaj-omsk.ru/strelki/">
                            Стрелка
                        </a>
                    </td>
                    <td>
                        3000                        руб.
                                            </td>
                </tr>

                <tr class="prices__main">
                    <td>
                        <a href="https://tatuaj-omsk.ru/gubi/">
                            Губы</a>:
                    </td>
                    <td>
                                            </td>
                </tr>
                <tr>
                    <td class="pl-30">
                        <a href="https://tatuaj-omsk.ru/kontur/">
                           Контур
                        </a>
                    </td>
                    <td>
                        2500                        руб.
                                                        <span class="sale">АКЦИЯ</span>
                                            </td>
                </tr>
                <tr>
                    <td class="pl-30">
                        <a href="https://tatuaj-omsk.ru/kontur-rastushevka/">
                           Контур с растушевкой
                        </a>
                    </td>
                    <td>
                        3500                        руб.
                                            </td>
                </tr>
                <tr>
                    <td class="pl-30">
                        <a href="https://tatuaj-omsk.ru/mushka/">
                           Мушка
                        </a>
                    </td>
                    <td>
                        500                        руб.
                                            </td>
                </tr>
                <tr class="prices__main">
                    <td>
                        Другое:
                    </td>
                    <td>

                    </td>
                </tr>
                <tr>
                    <td class="pl-30">
                        <a href="https://tatuaj-omsk.ru/korrektsiya/">
                           Коррекция татуажа
                        </a>
                    </td>
                    <td>
                        1500 - 2000 руб.
                    </td>
                </tr>
                <tr>
                    <td class="pl-30">
                        <a href="https://tatuaj-omsk.ru/obnovlenie-refresh/">
                            Обновление через 1 год (рефреш)
                        </a>
                    </td>
                    <td>
                        2500 руб.
                    </td>
                </tr>

                <tr>
                    <td class="pl-30">
                        <span class="js-open-popup gold">Консультация</span>
                    </td>
                    <td>бесплатно!</td>
                </tr>

            </tbody>
        </table>
    </div>
</div>