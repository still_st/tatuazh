<header>
    <div class="dark">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="pull-left">
                            <a class="top-bar__logo" href="/" class="button button-icon-clear" data-animate="fadeInUp" data-delay="300*index">
                                <img src="../img/logo.svg" alt="Лого">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 text-right">
                        <a href="tel:+78126029578" class="footer-label uppercase">
                            <span class="pull-right header-tel" data-animate="fadeInUp">+7 (812)602-95-78</span>
                            <span class="button button-icon-clear pull-right" data-animate="fadeInUp">
                                <i class="fa fa-phone"></i>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <section class="expandable">
            <a href="/" class="diamond">
                <span class="background"></span>
                <span class="line1"></span>
                <span class="line2"></span>
            </a>
            <div class="expandable-content dark">
                <div class="container padding-tq-top padding-minus-tq-bottom">


                    <div class="line">
                        <div class="col-4 col-m-12 col-s-12">
                            <div class="sub-heading">Популярные статьи</div>
                            <div class="iconed">
                                <i class="iconed-icon fa fa-book"></i>
                                <ul class="list list-sided">
                                    <li>
                                        <div class="stamp color-darker pull-right">05 Мая</div>
                                        <a href="#">Первая статья</a>
                                    </li>
                                    <li>
                                        <div class="stamp color-darker pull-right">06 Июня</div>
                                        <a href="#">Первая статья</a>
                                    </li>
                                    <li>
                                        <div class="stamp color-darker pull-right">27 Июля</div>
                                        <a href="#">Первая статья</a>
                                    </li>
                                    <li>
                                        <div class="stamp color-darker pull-right">29 Июля</div>
                                        <a href="#">Первая статья</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-4 col-m-12 col-s-12">
                            <div class="sub-heading"><a href="">Последние отзывы</a></div>
                            <div class="iconed">
                                <i class="iconed-icon fa fa-vk"></i>
                                <div class="roller">
                                    <ul class="list no-style roller-list">
                                        <li>
                                            <p class="small condensed">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod <a href="#">http://t.co/savannah</a> ut labore et dolore magna aliqua. Ut enim ad minim.</p>
                                            <p class="small condensed color-darker">35 minutes ago, <a href="#">@savannah</a></p>
                                        </li>
                                        <li>
                                            <p class="small condensed">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod <a href="#">http://t.co/savannah</a>.</p>
                                            <p class="small condensed color-darker">50 minutes ago, <a href="#">@savannah</a></p>
                                        </li>
                                        <li>
                                            <p class="small condensed">Lorem ipsum dolor sit amet, consectetur.</p>
                                            <p class="small condensed color-darker">2 days ago, <a href="#">@savannah</a></p>
                                        </li>
                                    </ul>
                                    <a href="#" class="button button-small button-icon roller-prev"><i class="fa fa-chevron-left"></i></a>
                                    <a href="#" class="button button-small button-icon roller-next"><i class="fa fa-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-4 col-m-12 col-s-12">
                            <div class="sub-heading">За работой</div>
                            <div class="iconed">
                                <i class="iconed-icon fa fa-flickr"></i>
                                <div class="flickr-widget">
                                    <div class="line">
                                        <div class="col-8">
                                            <div class="flickr-widget-item">
                                                <img src="img/work-process/min/work-process-1.jpg" alt="За работой 1"/>
                                            </div>
                                        </div>
                                        <div class="col-8">
                                            <div class="flickr-widget-item">
                                                <img src="img/work-process/min/work-process-2.jpg" alt="За работой 2"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </section>
    </div>

    <!-- Hero -->
    <div id="hero" class="hero" >
        <div class="container">
            <div class="content padding-top padding-tq-bottom text-center">
                <div class="hero-heading uppercase" data-animate="bounceIn" data-group="1"><span>Татуаж в Екатеринбурге</span></div>
                <div class="hero-subheading uppercase" data-animate="flipInY" data-delay="300" data-group="1" data-rotate-1="Beautiful person" data-rotate-2="Amateur guitar player" data-rotate-speed="5000" data-rotate-delay="0">Professional photographer</div>
                <div class="hero-amp" data-animate="flipInY" data-delay="600" data-group="1" data-rotate-1="&amp;" data-rotate-2="&amp;" data-rotate-speed="5000" data-rotate-delay="300">&amp;</div>
                <div class="hero-handscript lowercase" data-animate="flipInY" data-delay="900" data-group="1" data-rotate-1="People lover" data-rotate-2="Awesome story teller" data-rotate-speed="5000" data-rotate-delay="600">Graphic designer</div>
                <div class="cover-space hidden-sm hidden-xs"></div>
            </div>
        </div>
        <div class="cover hidden-sm hidden-xs" data-animate="fadeInDown" data-delay="350">
            <div class="one"></div>
            <div class="two"></div>
            <div class="header-ava">
                <img src="img/ava.jpg" alt="" />
            </div>
        </div>
    </div>

    <!-- Navigation -->
    <nav class="navbar mb-0" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="/" class="navbar-brand" data-animate="fadeInDown" data-delay="300"><span>Мастер татутажа</span></a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="#"><span>О нас<span class="tl"></span><span class="br"></span></span></a>
                    </li>
                    <li>
                        <a href="#"><span>Услуги<span class="tl"></span><span class="br"></span></span></a>
                    </li>
                    <li>
                        <a href="#"><span>Контакты<span class="tl"></span><span class="br"></span></span></a>
                    </li>
                    <li><a href="#contacts"><span>Цены<span class="tl"></span><span class="br"></span></span></a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>