<!-- Pricing Tables -->
<section id="pricing" class="padding-bottom text-center">
    <div class="section-heading" data-animate="bounceIn">Prices</div>
</section>
<div class="container padding-minus-bottom">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
            <div class="price-plan">
                <p class="price-plan-price" data-animate="fadeInDown"><strong>$9</strong> per month</p>
                <h2 class="price-plan-title" data-animate="fadeInRight">Economy</h2>
                <ul class="price-plan-features" data-animate="flipInY">
                    <li>Free Logo</li>
                    <li>Free Website</li>
                    <li class="unavailable">Ad Campaign</li>
                    <li class="unavailable">Lifetime Support</li>
                    <li class="unavailable">Professional Photoshoot</li>
                </ul>
                <a href="#" class="button button-icon-right" data-animate="fadeInUp">Order Now<i class="fa fa-long-arrow-right"></i></a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
            <div class="price-plan">
                <p class="price-plan-price" data-animate="fadeInDown"><strong>$19</strong> per month</p>
                <h2 class="price-plan-title" data-animate="fadeInRight">Standard</h2>
                <ul class="price-plan-features" data-animate="flipInY">
                    <li>Free Logo</li>
                    <li>Free Website</li>
                    <li>Ad Campaign</li>
                    <li class="unavailable">Lifetime Support</li>
                    <li class="unavailable">Professional Photoshoot</li>
                </ul>
                <a href="#" class="button button-icon-right" data-animate="fadeInUp">Order Now<i class="fa fa-long-arrow-right"></i></a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
            <div class="price-plan">
                <p class="price-plan-price" data-animate="fadeInDown"><strong>$29</strong> per month</p>
                <h2 class="price-plan-title" data-animate="fadeInRight">Business</h2>
                <ul class="price-plan-features" data-animate="flipInY">
                    <li>Free Logo</li>
                    <li>Free Website</li>
                    <li>Ad Campaign</li>
                    <li>Lifetime Support</li>
                    <li class="unavailable">Professional Photoshoot</li>
                </ul>
                <a href="#" class="button button-icon-right" data-animate="fadeInUp">Order Now<i class="fa fa-long-arrow-right"></i></a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
            <div class="price-plan">
                <p class="price-plan-price" data-animate="fadeInDown"><strong>$49</strong> per month</p>
                <h2 class="price-plan-title" data-animate="fadeInRight">Corporate</h2>
                <ul class="price-plan-features" data-animate="flipInY">
                    <li>Free Logo</li>
                    <li>Free Website</li>
                    <li>Ad Campaign</li>
                    <li>Lifetime Support</li>
                    <li>Professional Photoshoot</li>
                </ul>
                <a href="#" class="button button-icon-right" data-animate="fadeInUp">Order Now<i class="fa fa-long-arrow-right"></i></a>
            </div>
        </div>
    </div>
</div>