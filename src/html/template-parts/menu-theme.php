        <div class="padding-bottom text-center bb">
            <h1 class="section-heading" data-animate="bounceIn">Услуги</h1>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="blog-post-small">
                        <img src="img/blog1.jpg" data-animate="fadeInRight" class="image" alt="" />
                        <div class="info" data-animate="fadeInRight">
                            <div class="diamond" data-animate="fadeInUp">
                                <div class="background"></div>
                                <div class="icon"><i class="fa fa-picture-o"></i></div>
                            </div>
                            <div class="arrow"></div>
                            <div class="stats clearfix">
                                <div class="stat uppercase pull-left" data-animate="fadeInUp" data-delay="300*index+300"><i class="fa fa-calendar"></i>12 Feb</div>
                                <div class="stat uppercase pull-left" data-animate="fadeInUp" data-delay="300*index+300"><i class="fa fa-eye"></i>1,345</div>
                                <div class="stat uppercase pull-left" data-animate="fadeInUp" data-delay="300*index+300"><i class="fa fa-comments-o"></i>113</div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="sub-heading" data-animate="flipInY" data-delay="300" data-group="2">My trip to New York</div>
                            <p data-animate="flipInY" data-delay="600" data-group="2">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua&hellip;</p>
                        </div>
                        <a href="./single-post.html" class="button button-small" data-animate="fadeInUp" data-delay="900" data-group="1">Read more</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="blog-post-small">
                        <img src="img/blog2.jpg" data-animate="fadeInRight" class="image" alt="" />
                        <div class="info" data-animate="fadeInRight">
                            <div class="diamond" data-animate="fadeInUp">
                                <div class="background"></div>
                                <div class="icon"><i class="fa fa-picture-o"></i></div>
                            </div>
                            <div class="arrow"></div>
                            <div class="stats clearfix">
                                <div class="stat uppercase pull-left" data-animate="fadeInUp" data-delay="300*index+300"><i class="fa fa-calendar"></i>12 Feb</div>
                                <div class="stat uppercase pull-left" data-animate="fadeInUp" data-delay="300*index+300"><i class="fa fa-eye"></i>1,345</div>
                                <div class="stat uppercase pull-left" data-animate="fadeInUp" data-delay="300*index+300"><i class="fa fa-comments-o"></i>113</div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="sub-heading" data-animate="flipInY" data-delay="300" data-group="2">Festive french streets</div>
                            <p data-animate="flipInY" data-delay="600" data-group="2">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua&hellip;</p>
                        </div>
                        <a href="./single-post.html" class="button button-small" data-animate="fadeInUp" data-delay="900" data-group="1">Read more</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="blog-post-small">
                        <img src="img/blog3.jpg" data-animate="fadeInRight" class="image" alt="" />
                        <div class="info" data-animate="fadeInRight">
                            <div class="diamond" data-animate="fadeInUp">
                                <div class="background"></div>
                                <div class="icon"><i class="fa fa-picture-o"></i></div>
                            </div>
                            <div class="arrow"></div>
                            <div class="stats clearfix">
                                <div class="stat uppercase pull-left" data-animate="fadeInUp" data-delay="300*index+300"><i class="fa fa-calendar"></i>12 Feb</div>
                                <div class="stat uppercase pull-left" data-animate="fadeInUp" data-delay="300*index+300"><i class="fa fa-eye"></i>1,345</div>
                                <div class="stat uppercase pull-left" data-animate="fadeInUp" data-delay="300*index+300"><i class="fa fa-comments-o"></i>113</div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="sub-heading" data-animate="flipInY" data-delay="300" data-group="2">My new workstation!</div>
                            <p data-animate="flipInY" data-delay="600" data-group="2">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua&hellip;</p>
                        </div>
                        <a href="./single-post.html" class="button button-small" data-animate="fadeInUp" data-delay="900" data-group="1">Read more</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="blog-post-small">
                        <img src="img/blog4.jpg" data-animate="fadeInRight" class="image" alt="" />
                        <div class="info" data-animate="fadeInRight">
                            <div class="diamond" data-animate="fadeInUp">
                                <div class="background"></div>
                                <div class="icon"><i class="fa fa-picture-o"></i></div>
                            </div>
                            <div class="arrow"></div>
                            <div class="stats clearfix">
                                <div class="stat uppercase pull-left" data-animate="fadeInUp" data-delay="300*index+300"><i class="fa fa-calendar"></i>12 Feb</div>
                                <div class="stat uppercase pull-left" data-animate="fadeInUp" data-delay="300*index+300"><i class="fa fa-eye"></i>1,345</div>
                                <div class="stat uppercase pull-left" data-animate="fadeInUp" data-delay="300*index+300"><i class="fa fa-comments-o"></i>113</div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="sub-heading" data-animate="flipInY" data-delay="300" data-group="2">Quick lunch!</div>
                            <p data-animate="flipInY" data-delay="600" data-group="2">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua&hellip;</p>
                        </div>
                        <a href="./single-post.html" class="button button-small" data-animate="fadeInUp" data-delay="900" data-group="1">Read more</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="blog-post-small">
                        <img src="img/blog1.jpg" data-animate="fadeInRight" class="image" alt="" />
                        <div class="info" data-animate="fadeInRight">
                            <div class="diamond" data-animate="fadeInUp">
                                <div class="background"></div>
                                <div class="icon"><i class="fa fa-picture-o"></i></div>
                            </div>
                            <div class="arrow"></div>
                            <div class="stats clearfix">
                                <div class="stat uppercase pull-left" data-animate="fadeInUp" data-delay="300*index+300"><i class="fa fa-calendar"></i>12 Feb</div>
                                <div class="stat uppercase pull-left" data-animate="fadeInUp" data-delay="300*index+300"><i class="fa fa-eye"></i>1,345</div>
                                <div class="stat uppercase pull-left" data-animate="fadeInUp" data-delay="300*index+300"><i class="fa fa-comments-o"></i>113</div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="sub-heading" data-animate="flipInY" data-delay="300" data-group="2">My trip to New York</div>
                            <p data-animate="flipInY" data-delay="600" data-group="2">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua&hellip;</p>
                        </div>
                        <a href="./single-post.html" class="button button-small" data-animate="fadeInUp" data-delay="900" data-group="1">Read more</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="blog-post-small">
                        <img src="img/blog2.jpg" data-animate="fadeInRight" class="image" alt="" />
                        <div class="info" data-animate="fadeInRight">
                            <div class="diamond" data-animate="fadeInUp">
                                <div class="background"></div>
                                <div class="icon"><i class="fa fa-picture-o"></i></div>
                            </div>
                            <div class="arrow"></div>
                            <div class="stats clearfix">
                                <div class="stat uppercase pull-left" data-animate="fadeInUp" data-delay="300*index+300"><i class="fa fa-calendar"></i>12 Feb</div>
                                <div class="stat uppercase pull-left" data-animate="fadeInUp" data-delay="300*index+300"><i class="fa fa-eye"></i>1,345</div>
                                <div class="stat uppercase pull-left" data-animate="fadeInUp" data-delay="300*index+300"><i class="fa fa-comments-o"></i>113</div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="sub-heading" data-animate="flipInY" data-delay="300" data-group="2">Festive french streets</div>
                            <p data-animate="flipInY" data-delay="600" data-group="2">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua&hellip;</p>
                        </div>
                        <a href="./single-post.html" class="button button-small" data-animate="fadeInUp" data-delay="900" data-group="1">Read more</a>
                    </div>
                </div>
            </div>
            <div class="text-center padding-minus-bottom">
                <a href="./posts-big.html" class="button button-icon-left" data-animate="flipInY"><i class="fa fa-book"></i>More from the blog</a>
            </div>
        </div>