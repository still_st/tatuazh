<div class="order">
    <div class="padding-tq-top padding-minus-tq-bottom text-center">
        <div class="content">
            <div class="container">
                <a href="#" class="button button-big js-open-popup" data-animate="flipInY" data-group="1">
                    <span class="icon"><i class="fa fa-envelope"></i></span>
                    Написать нам
                </a>
            </div>
        </div>
    </div>
</div>