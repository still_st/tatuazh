<div class="bb galery certificate" id="certificate">
    <div class="wrapper tac">
        <h2 class="hero-heading section-heading-my bounceIn animated">
            Сертификаты
        </h2>
        <div class="line galery__portfolio">
            <div class="col-3 col-m-6 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a class="galery__img-link" href="img/certificates/3d_lashes_certificate.jpg">
                       <img src="img/certificates/min/3d_lashes_certificate.jpg" alt="сертификат брови 3d">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 grid tatuazh">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/certificates/BastetStyle.jpg">
                       <img src="img/certificates/min/BastetStyle.jpg" alt="Сертификат о прохождении курса об идеальных бровях">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 grid tatuazh">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/certificates/beaty_lashes.jpg">
                       <img src="img/certificates/min/beaty_lashes.jpg" alt="Сертификат наращивании ресниц beaty lashes">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 grid tatuazh">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/certificates/certifiacte_college.jpg">
                       <img src="img/certificates/min/certifiacte_college.jpg" alt="сертификат художников по макияжу">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/certificates/certificate_kate_mur.jpg">
                       <img src="img/certificates/min/certificate_kate_mur.jpg" alt="сертификат современный макияж">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/certificates/certificate-femme-fatale.jpg">
                       <img src="img/certificates/min/certificate-femme-fatale.jpg" alt="Сертификат перманентный макияж">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/certificates/certificate-makeup.jpg">
                       <img src="img/certificates/min/certificate-makeup.jpg" alt="Сертификат make up professional">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
             <div class="col-3 col-m-6 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/certificates/certificate-studio-raise.jpg">
                       <img src="img/certificates/min/certificate-studio-raise.jpg" alt="Сертификат креативный макияж">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
             <div class="col-3 col-m-6 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/certificates/creative_makeup.jpg">
                       <img src="img/certificates/min/creative_makeup.jpg" alt="Сертификат креативный макияж">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
             <div class="col-3 col-m-6 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/certificates/diplom.jpg">
                       <img src="img/certificates/min/diplom.jpg" alt="Диплом перманентный макияж">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
             <div class="col-3 col-m-6 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/certificates/makeup_school.jpg">
                       <img src="img/certificates/min/makeup_school.jpg" alt="Сертификат трансформирование образа">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/certificates/vizaj.jpg">
                       <img src="img/certificates/min/vizaj.jpg" alt="Сертификат визаж">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
        </div>
    </div>
</div>