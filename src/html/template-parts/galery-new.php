<div class="bb galery">
    <div class="wrapper tac">
        <h2 class="section-heading" data-animate="bounceIn">
            Отзывы в сообщениях
        </h2>

        <div class="portfolio-filters">
            <span class="portfolio-filter" id="*">Все</span>
            <span class="portfolio-filter" id="tatuazh">Татуаж</span>
        </div>

        <div class="line galery__portfolio">
            <div class="col-3 col-m-6 col-s-12 grid tatuazh">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Алёна Серафимова.jpg">
                       <img src="img/feedbacks/min/Алёна Серафимова.jpg" alt="Отзыв Алёны Серафимовой">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 col-s-12 grid tatuazh">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Анастасия Лепокурова.jpg">
                       <img src="img/feedbacks/min/Анастасия Лепокурова.jpg" alt="Отзыв Анастасии Лепокуровой">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 col-s-12 grid tatuazh">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Анна Андреева.jpg">
                       <img src="img/feedbacks/min/Анна Андреева.jpg" alt="Отзыв Анны Андреевой">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Анна Флигина.jpg">
                       <img src="img/feedbacks/min/Анна Флигина.jpg" alt="Отзыв Анны Флигиной">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Аня Куликова.jpg">
                       <img src="img/feedbacks/min/Аня Куликова.jpg" alt="Отзыв Ани Куликовой">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Аня Тычинская.jpg">
                       <img src="img/feedbacks/min/Аня Тычинская.jpg" alt="Отзыв Ани Тычинской">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Аполония Прокофьева.jpg">
                       <img src="img/feedbacks/min/Аполония Прокофьева.jpg" alt="Отзыв Аполонии Прокофьевой">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
             <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/ЕвгенияВороьёва.jpg">
                       <img src="img/feedbacks/min/ЕвгенияВороьёва.jpg" alt="Отзыв Евгении Воробевой">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
             <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Екатерина Беляева.jpg">
                       <img src="img/feedbacks/min/Екатерина Беляева.jpg" alt="Отзыв Екатерины Беляевой">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
             <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Екатерина Карим.jpg">
                       <img src="img/feedbacks/min/Екатерина Карим.jpg" alt="Отзыв Екатерины Карим">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
             <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Ирина Шарофеева.jpg">
                       <img src="img/feedbacks/min/Ирина Шарофеева.jpg" alt="Отзыв Ирины Шарофеевой">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
             <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Катюшка Злобина.jpg">
                       <img src="img/feedbacks/min/Катюшка Злобина.jpg" alt="Отзыв Катюшки Злобиной">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
             <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Ксения Масленникова.jpg">
                       <img src="img/feedbacks/min/Ксения Масленникова.jpg" alt="Отзыв Ксении Масленниковой">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
             <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Ксюша Шинакова.jpg">
                       <img src="img/feedbacks/min/Ксюша Шинакова.jpg" alt="Отзыв Ксюши Шинаковой">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Леночка.jpg">
                       <img src="img/feedbacks/min/Леночка.jpg" alt="Отзыв Леночки">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Лерочка Томилова.jpg">
                       <img src="img/feedbacks/min/Лерочка Томилова.jpg" alt="Отзыв Лерочки Томиловой">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Любовь Чингвинцева.jpg">
                       <img src="img/feedbacks/min/Любовь Чингвинцева.jpg" alt="Отзыв Любви Чингвинцевой">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Марина Вовк.jpg">
                       <img src="img/feedbacks/min/Марина Вовк.jpg" alt="Отзыв Марины Вовк">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Марина Невьянцева.jpg">
                       <img src="img/feedbacks/min/Марина Невьянцева.jpg" alt="Отзыв Марины Невьянцевой">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Маришка Павлыго.jpg">
                       <img src="img/feedbacks/min/Маришка Павлыго.jpg" alt="Отзыв Маришки Павлыго">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Мария Гильмиярова.jpg">
                       <img src="img/feedbacks/min/Мария Гильмиярова.jpg" alt="Отзыв Марии Гильмияровой">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Наталья Афанасьева.jpg">
                       <img src="img/feedbacks/min/Наталья Афанасьева.jpg" alt="Отзыв Натальи Афанасьевой">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Наталья Чудинова.jpg">
                       <img src="img/feedbacks/min/Наталья Чудинова.jpg" alt="Отзыв Натальи Чудиновой">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Нина Зверева.jpg">
                       <img src="img/feedbacks/min/Нина Зверева.jpg" alt="Отзыв Нины Зверевой">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Ольга Исакаева.jpg">
                       <img src="img/feedbacks/min/Ольга Исакаева.jpg" alt="Отзыв Ольги Исакаевой">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Ольга Шу.jpg">
                       <img src="img/feedbacks/min/Ольга Шу.jpg" alt="Отзыв Ольги Шу">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Отзыв.jpg">
                       <img src="img/feedbacks/min/Отзыв.jpg" alt="Отзыв о татуаже">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Рина Рокко.jpg">
                       <img src="img/feedbacks/min/Рина Рокко.jpg" alt="Отзыв Рины Рокко">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Татьяна Кузнецова.jpg">
                       <img src="img/feedbacks/min/Татьяна Кузнецова.jpg" alt="Отзы Татьяна Кузнецовой">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Юлия Чеснокова.jpg">
                       <img src="img/feedbacks/min/Юлия Чеснокова.jpg" alt="Отзы Юлия Чесноковой">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Яна Шакирова.jpg">
                       <img src="img/feedbacks/min/Яна Шакирова.jpg" alt="Отзыв Яны Шакировой">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3 col-m-6 col-s-12 grid">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/feedbacks/Яна.jpg">
                       <img src="img/feedbacks/min/Яна.jpg" alt="Отзыв Яны">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
        </div>

        <a class="galery__link" href="#">
            Смотреть все работы
        </a>
    </div>
</div>