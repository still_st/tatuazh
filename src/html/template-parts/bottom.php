        	<!-- JS libraries -->


            <!-- build:js js/myscript.js -->
                            <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
                            <script type="text/javascript" src="js/bootstrap.min.js"></script>
                            <script type="text/javascript" src="js/jquery.rotator.js"></script>

                            <script type="text/javascript" src="js/jquery.bez.min.js"></script>
                            <script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
                            <script type="text/javascript" src="js/jquery.scrollTo-1.4.3.1-min.js"></script>
                            <!--<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>-->

                            <!-- Initiate scroll show effects, remove if you do not want the effects
                            <script type="text/javascript" src="js/init-scroll.js"></script>
                            -->
                            <!-- Theme specific scripts -->
                            <script type="text/javascript" src="js/scripts.js"></script>

                            <script type="text/javascript" src="js/countUp.min.js"></script>
                            <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
                            <script type="text/javascript" src="bower/owl.carousel/dist/owl.carousel.min.js"></script>

                            <script type="text/javascript" src="js/script.js"></script>
            <!-- endbuild -->


    </body>
</html>