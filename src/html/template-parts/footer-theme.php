<!-- Footer section -->
<footer class="padding-tq-top padding-minus-tq-bottom text-center">
    <a href="#hero" class="top"><i class="fa fa-chevron-up"></i></a>
    <h4 class="colored" data-animate="fadeInUp">Savannah May</h4>
    <a href="https://www.facebook.com/tony.bogdanov.9" class="button button-icon" data-animate="fadeInUp" data-delay="300*index-300" data-group="1"><i class="fa fa-facebook"></i></a>
    <a href="https://twitter.com/tony_bogdanov" class="button button-icon" data-animate="fadeInUp" data-delay="300*index-300" data-group="1"><i class="fa fa-twitter"></i></a>
    <a href="https://plus.google.com/104315383192649358641" class="button button-icon" data-animate="fadeInUp" data-delay="300*index-300" data-group="1"><i class="fa fa-google-plus"></i></a>
    <a href="#" class="button button-icon" data-animate="fadeInUp" data-delay="300*index-300"><i class="fa fa-pinterest" data-group="1"></i></a>
    <a href="#" class="button button-icon" data-animate="fadeInUp" data-delay="300*index-300"><i class="fa fa-dribbble" data-group="1"></i></a>
    <div class="copyright colored" data-animate="fadeInUp" data-delay="300*index-300" data-group="1">&copy;2014 All rights reserved. Made with <i class="fa fa-heart"></i> by <a href="http://tonybogdanov.com" target="_blank" class="colored">TonyBogdanov</a></div>
</footer>