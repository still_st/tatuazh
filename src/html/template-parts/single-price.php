<section class="bb prices" id="prices" itemscope itemtype="http://schema.org/Table">
    <div class="wrapper">
        <div class="text-center">
            <h2 class="hero-heading section-heading-my bounceIn animated" itemprop="about">Цены</h2>
            <table>
                <thead>
                    <tr>
                        <th>Услуга</th>
                        <th>Стоимость</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="prices__title">Микроблейдинг</td>
                        <td>6000 руб.</td>
                    </tr>
                    <tr>
                        <td>Микроблейдинг</td>
                        <td>6000 руб.</td>
                    </tr>
                    <tr>
                        <td>Растушевка</td>
                        <td>5500 руб.</td>
                    </tr>
                    <tr>
                        <td>Волоски</td>
                        <td>5500 руб. <span class="sale">АКЦИЯ</span></td>
                    </tr>
                    <tr>
                        <td>Пудровая (акварель /шатуш)</td>
                        <td>6000 руб. <span class="sale">АКЦИЯ</span></td>
                    </tr>
                    <tr>
                        <td>Волоски с растушевкой</td>
                        <td>6000 руб.</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>