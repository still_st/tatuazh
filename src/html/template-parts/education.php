<!-- Work and education section -->
        <section class="padding text-center">
            <div class="section-heading" data-animate="bounceIn">Work &amp; education</div>
        </section>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 padding-minus-bottom">
                    <ul class="history">
                        <li>
                            <div class="heading">
                                <div class="title" data-animate="fadeInRight" data-group="2">Lead Photographer</div>
                                <div class="icon" data-animate="bounceIn" data-group="2"><i class="fa fa-briefcase"></i></div>
                                <div class="datetime" data-animate="fadeInUp" data-delay="300" data-group="2">Dec 2005 &ndash; Nov 2013</div>
                                <div class="aff uppercase" data-animate="fadeInUp" data-delay="600" data-group="2">Fotoshoot Magazine</div>
                            </div>
                            <p data-animate="flipInY" data-delay="300*index+600" data-group="1">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </li>
                        <li>
                            <div class="heading">
                                <div class="title" data-animate="fadeInRight" data-group="2">Junior Photographer</div>
                                <div class="icon" data-animate="bounceIn" data-group="2"><i class="fa fa-briefcase"></i></div>
                                <div class="datetime" data-animate="fadeInUp" data-delay="300" data-group="2">Jan 2004 &ndash; Jan 2005</div>
                                <div class="aff uppercase" data-animate="fadeInUp" data-delay="600" data-group="2">ABC Model Agency</div>
                            </div>
                            <p data-animate="flipInY" data-delay="300*index+600" data-group="1">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </li>
                        <li>
                            <div class="heading">
                                <div class="title" data-animate="fadeInRight" data-group="2">Graphic Designer</div>
                                <div class="icon" data-animate="bounceIn" data-group="2"><i class="fa fa-briefcase"></i></div>
                                <div class="datetime" data-animate="fadeInUp" data-delay="300" data-group="2">Jun 2000 &ndash; Dec 2003</div>
                                <div class="aff uppercase" data-animate="fadeInUp" data-delay="600" data-group="2">FXMedia Ltd.</div>
                            </div>
                            <p data-animate="flipInY" data-delay="300*index+600" data-group="1">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 padding-minus-bottom">
                    <ul class="history">
                        <li>
                            <div class="heading">
                                <div class="title" data-animate="fadeInRight" data-delay="900" data-group="2">Art History</div>
                                <div class="icon" data-animate="bounceIn" data-delay="900" data-group="2"><i class="fa fa-book"></i></div>
                                <div class="datetime" data-animate="fadeInUp" data-delay="1200" data-group="2">Sep 2000 &ndash; Aug 2012</div>
                                <div class="aff uppercase" data-animate="fadeInUp" data-delay="1500" data-group="2">Masters degree, Harvard</div>
                            </div>
                            <p data-animate="flipInY" data-delay="300*index+1500" data-group="1">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </li>
                        <li>
                            <div class="heading">
                                <div class="title" data-animate="fadeInRight" data-delay="900" data-group="2">Photography &amp; Design</div>
                                <div class="icon" data-animate="bounceIn" data-delay="900" data-group="2"><i class="fa fa-book"></i></div>
                                <div class="datetime" data-animate="fadeInUp" data-delay="1200" data-group="2">Sep 1996 &ndash; Jun 2000</div>
                                <div class="aff uppercase" data-animate="fadeInUp" data-delay="1500" data-group="2">Bachelors degree, Harvard</div>
                            </div>
                            <p data-animate="flipInY" data-delay="300*index+1500" data-group="1">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </li>
                        <li>
                            <div class="heading">
                                <div class="title" data-animate="fadeInRight" data-delay="900" data-group="2">Fine Arts</div>
                                <div class="icon" data-animate="bounceIn" data-delay="900" data-group="2"><i class="fa fa-book"></i></div>
                                <div class="datetime" data-animate="fadeInUp" data-delay="1200" data-group="2">Sep 1991 &ndash; May 1996</div>
                                <div class="aff uppercase" data-animate="fadeInUp" data-delay="1500" data-group="2">Van Gogh High, Massachusetts</div>
                            </div>
                            <p data-animate="flipInY" data-delay="300*index+1500" data-group="1">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>