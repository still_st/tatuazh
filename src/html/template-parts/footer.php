<footer class="footer">
    <div class="wrapper">
        <div class="line">
            <div class="col-4 col-m-6 col-s-6 footer__info">
                <h3>Информация</h3>
                <span>
                    <a class="footer__link" href="otzivi/">Отзывы</a>
                </span>
                <span>
                    <a class="footer__link" href="blog/">Блог</a>
                </span>
                <span>
                    <a class="footer__link" href="vakansii/">Вакансии</a>
                </span>
                <span>
                    <a class="footer__link" href="sitemap/">Карта сайта</a>
                </span>
                <span>
                    <a class="footer__link" href="sitemap/">Политика конфиденциальности</a>
                </span>
            </div>
            <div class="col-4 col-m-6 col-s-6">
                <div class="footer__insta">
                    <h3><a href="https://www.instagram.com/permanentmakeup_spb/">Инстаграм</a></h3>

                    <a href="https://www.instagram.com/p/BTlJqM6lHQE/" target="_blank"><img src="../img/insta/1.jpg" alt="Работа в профиле instagram"></a>
                    <a href="https://www.instagram.com/p/BOUjFU1BMqo/" target="_blank"><img src="../img/insta/2.jpg" alt="Работа в профиле instagram"></a>
                    <a href="https://www.instagram.com/p/BOUi4OYBWz1/" target="_blank"><img src="../img/insta/3.jpg" alt="Работа в профиле instagram"></a>
                    <a href="https://www.instagram.com/p/BOUiyRQhX4X/" target="_blank"><img src="../img/insta/4.jpg" alt="Работа в профиле instagram"></a>
                    <a href="https://www.instagram.com/p/BOUisZyBJfp/" target="_blank"><img src="../img/insta/5.jpg" alt="Работа в профиле instagram"></a>
                    <a href="https://www.instagram.com/p/BOUimOXBLcY/" target="_blank" ><img src="../img/insta/6.jpg" alt="Работа в профиле instagram"></a>
                    <a href="https://www.instagram.com/p/BOUiefHBBdX/" target="_blank" ><img src="../img/insta/7.jpg" alt="Работа в профиле instagram"></a>
                    <a href="https://www.instagram.com/p/BOUh85QBaTo/" target="_blank" ><img src="../img/insta/8.jpg" alt="Работа в профиле instagram"></a>
                    <a href="https://www.instagram.com/p/BOUh1QbB7j8/" target="_blank" ><img src="../img/insta/9.jpg" alt="Работа в профиле instagram"></a>
                </div>

            </div>
            <div class="col-4 col-m-6 col-s-6 footer__place" itemscope itemtype="http://schema.org/BeautySalon">
                <h3>Контакты</h3>

                <span class ="footer__name" itemprop="name">Мастер татуажа</span>

                <span class="footer__tel" >
                    <a href="tel:+78126029578" itemprop="telephone">+7 (812) 602-95-78</a>
                </span>

                <div class="footer__hours" >
                    <time itemprop="openingHours" datetime="Mo-Su 10:00−22:00">
                        <span>Звоните нам с 10:00 до 22:00</span>
                        <span>7 дней в неделю</span>
                    </time>
                </div>

                <div class="footer__adress" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                    <div>
                        <span itemprop="addressLocality">г. Санкт-Петербург</span>
                        <span itemprop="streetAddress">20-я линия В.О., дом 9</span>
                    </div>
                </div>

                <span class="footer__mail">
                    <a href="mailto:info@master-tatuazha.ru" itemprop="email">info@master-tatuazha.ru
                    </a>
                </span>

                <span class="footer__insta-link">
                    <a href="https://www.instagram.com/permanentmakeup_spb/" target="_blank">permanentmakeup_spb</a>
                </span>
            </div>

        </div>
    </div>
    <div class="footer__bottom">
        <div class="wrapper">
            <p>
                Внимание! Данный сайт носит исключительно информационный характер и не является публичной офертой, определяемой положениями Статьи 437 Гражданского Кодекса РФ.
            </p>
        </div>
    </div>

</footer>