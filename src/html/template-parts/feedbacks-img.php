<section class="feed" id="feed" data-scroll-speed="400">
    <div class="padding-tq-top padding-minus-tq-bottom">
        <div class="content">
            <div class="container">

                <div class="text-center">
                    <p class="hero-heading section-heading-my bounceIn animated">
                        <a href="<?php echo get_page_link(13);?>">Отзывы</a>
                    </p>
                </div>

            <div class="wrapper video">
                <ul id="owl-carousel--feed" class="owl-carousel">
                    <li class="owl-carousel__item">
                        <div class="galery__img">
                            <div class="galery__img-l"></div>
                            <a class="galery__img-link" href="img/feedbacks/1.jpg">
                               <img src="img/feedbacks/min/1.jpg" alt="отзыв Александры Суровцевой">
                            </a>
                            <div class="galery__img-r"></div>
                        </div>
                    </li>
                    <li class="owl-carousel__item">
                        <div class="galery__img">
                            <div class="galery__img-l"></div>
                            <a class="galery__img-link" href="img/feedbacks/1.jpg">
                               <img src="img/feedbacks/min/1.jpg" alt="отзыв Александры Суровцевой">
                            </a>
                            <div class="galery__img-r"></div>
                        </div>
                    </li>
                    <li class="owl-carousel__item">
                        <div class="galery__img">
                            <div class="galery__img-l"></div>
                            <a class="galery__img-link" href="/img/feedbacks/1.jpg">
                               <img src="/img/feedbacks/min/1.jpg" alt="отзыв Александры Суровцевой">
                            </a>
                            <div class="galery__img-r"></div>
                        </div>
                    </li>
                    <li class="owl-carousel__item">
                        <div class="galery__img">
                            <div class="galery__img-l"></div>
                            <a class="galery__img-link" href="img/feedbacks/1.jpg">
                               <img src="img/feedbacks/min/1.jpg" alt="отзыв Александры Суровцевой">
                            </a>
                            <div class="galery__img-r"></div>
                        </div>
                    </li>
                    <li class="owl-carousel__item">
                        <div class="galery__img">
                            <div class="galery__img-l"></div>
                            <a class="galery__img-link" href="img/feedbacks/1.jpg">
                               <img src="img/feedbacks/min/1.jpg" alt="отзыв Александры Суровцевой">
                            </a>
                            <div class="galery__img-r"></div>
                        </div>
                    </li>
                    <li class="owl-carousel__item">
                        <div class="galery__img">
                            <div class="galery__img-l"></div>
                            <a class="galery__img-link" href="img/feedbacks/1.jpg">
                               <img src="img/feedbacks/min/1.jpg" alt="отзыв Александры Суровцевой">
                            </a>
                            <div class="galery__img-r"></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>