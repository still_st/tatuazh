<div class="certificate bb" id="certificate">
    <div class="wrapper">
        <h2 class="hero-heading section-heading-my bounceIn animated">
            Сертификаты
        </h2>
        <div class="line">
            <div class="col-3">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/certificates/certificate-1.jpg">
                       <img src="img/certificates/min/certificate-1.jpg" alt="Сертификат биотатуаж перманентный макияж">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/certificates/certificate-2.jpg">
                       <img src="img/certificates/min/certificate-2.jpg" alt="Группа обучения биотатуажу и перманентному макияжу">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-3">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/certificates/certificate-3.jpg">
                       <img src="img/certificates/min/certificate-3.jpg" alt="Сертификат биотатуаж перманентный макияж 2">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<a href="<?php echo get_page_link(8);?>">ссылке</a>