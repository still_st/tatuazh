<!-- Extra info - expandable section -->
        <section class="expandable">
            <a href="#" class="diamond">
                <span class="background"></span>
                <span class="line1"></span>
                <span class="line2"></span>
            </a>
            <div class="expandable-content dark coloredbg">
                <div class="container padding-tq-top padding-minus-tq-bottom">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <div class="sub-heading white">Extra info</div>
                            <div class="iconed">
                                <i class="iconed-icon fa fa-database"></i>
                                <p class="white">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                <a href="#" class="button button-small button-inverted">Learn more</a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <div class="sub-heading white">Extra info</div>
                            <div class="iconed">
                                <i class="iconed-icon fa fa-gift"></i>
                                <p class="white">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                <a href="#" class="button button-small button-inverted">Learn more</a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <div class="sub-heading white">Extra info</div>
                            <div class="iconed">
                                <i class="iconed-icon fa fa-graduation-cap"></i>
                                <p class="white">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                <a href="#" class="button button-small button-inverted">Learn more</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>