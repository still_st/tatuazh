<div class="portfoilo-link bt">
    <div class="padding-tq-top padding-minus-tq-bottom text-center">
        <div class="content">
            <div class="container">
                <a href="<?php echo get_page_link(20);?>" class="button button-big js-open-popup" data-animate="flipInY" data-group="1">
                    <span class="icon"><i class="fa fa-envelope"></i></span>
                    Моё портфолио
                </a>
            </div>
        </div>
    </div>
</div>