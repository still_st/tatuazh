<div class="bb work-process" id="workProcess">
    <div class="wrapper">
        <h2 class="hero-heading section-heading-my bounceIn animated">
            За работой
        </h2>
        <div class="line">
            <div class="col-6 col-s-12">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/work-process/work-process-1.jpg">
                       <img src="img/work-process/work-process-1.jpg" alt="Лаврова Алёна за работой 1">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
            <div class="col-6 col-s-12">
                <div class="galery__img">
                    <div class="galery__img-l"></div>
                    <a  class="galery__img-link" href="img/work-process/work-process-2.jpg">
                       <img src="img/work-process/work-process-2.jpg" alt="Лаврова Алёна за работой 2">
                    </a>
                    <div class="galery__img-r"></div>
                </div>
            </div>
        </div>
    </div>
</div>