<section class="feed" id="feed" data-scroll-speed="400">
    <div class="padding-tq-top padding-minus-tq-bottom">
        <div class="content">
            <div class="container">

                <div class="text-center">
                    <p class="hero-heading section-heading-my bounceIn animated">
                        <a href="<?php echo get_page_link(13);?>">Отзывы</a>
                    </p>
                </div>

                <div class="row">

                    <div class="col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-lg-6 col-md-6 col-sm-6">
                        <div class="testimonials roller" data-animate="flipInY">
                            <div class="testimonials-box clearfix">
                                <ul class="list no-list-margin no-style roller-list">
                                    <li class="clearfix">
                                        <img src="img/testimonial.jpg" class="testimonials-image pull-right" alt="" />
                                        <div class="testimonials-text">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        </div>

                                        <a class="testimonials-name" href="#">Бодрова Света</a>
                                    </li>
                                    <li class="clearfix">
                                        <img src="img/testimonial.jpg" class="testimonials-image pull-right" alt="" />
                                        <div class="testimonials-text">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                                        </div>
                                        <a class="testimonials-name" href="#">Бодрова Света</a>
                                    </li>
                                    <li class="clearfix">
                                        <img src="img/testimonial.jpg" class="testimonials-image pull-right" alt="" />
                                        <div class="testimonials-text">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                        </div>
                                        <a class="testimonials-name" href="#">Бодрова Света</a>
                                    </li>
                                </ul>
                                <span class="testimonials-box__links">
                                    <a href="#">Читать все отзывы</a>
                                </span>
                            </div>
                            <div class="testimonials-bullets roller-bullets"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wrapper">
                <ul class="video owl-carousel">
                    <li class="owl-carousel__item">
                        <iframe src="https://www.youtube.com/embed/Uj6C53OOx9A" allowfullscreen></iframe>
                    </li>
                    <li class="owl-carousel__item">
                        <iframe src="https://www.youtube.com/embed/JqYe06_2vkc" allowfullscreen></iframe>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>