<!-- My skills section -->
<section class="padding-bottom text-center">
    <div class="section-heading" data-animate="bounceIn">My skills</div>
</section>
<div class="container">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4 padding-minus-bottom">
            <div class="sub-heading" data-animate="fadeInRight" data-group="1">Graphic skills</div>
            <label data-animate="fadeInRight" data-delay="300" data-group="1">Photoshop</label>
            <div class="skill-bar compact" data-value="30%" data-animate="fadeInRight" data-delay="600" data-group="1">
                <div class="bar">
                    <div class="fill">
                        <div class="value">0%</div>
                    </div>
                </div>
            </div>
            <label data-animate="fadeInRight" data-delay="900" data-group="1">Illustrator</label>
            <div class="skill-bar compact" data-value="45%" data-animate="fadeInRight" data-delay="1200" data-group="1">
                <div class="bar">
                    <div class="fill">
                        <div class="value">0%</div>
                    </div>
                </div>
            </div>
            <label data-animate="fadeInRight" data-delay="1500" data-group="1">After Effects</label>
            <div class="skill-bar compact" data-value="75%" data-animate="fadeInRight" data-delay="1800" data-group="1">
                <div class="bar">
                    <div class="fill">
                        <div class="value">0%</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 padding-minus-bottom">
            <div class="sub-heading" data-animate="fadeInRight" data-group="1">Language skills</div>
            <label data-animate="fadeInRight" data-delay="300" data-group="1">English</label>
            <div class="skill-bar skill-bar-bullets compact" data-value="95%" data-animate="fadeInRight" data-delay="600" data-group="1">
                <div class="bar">
                    <div class="fill"></div>
                    <div class="fill"></div>
                    <div class="fill"></div>
                    <div class="fill"></div>
                    <div class="fill"></div>
                    <div class="fill"></div>
                    <div class="fill"></div>
                </div>
            </div>
            <label data-animate="fadeInRight" data-delay="300" data-group="1">German</label>
            <div class="skill-bar skill-bar-bullets compact" data-value="55%" data-animate="fadeInRight" data-delay="1200" data-group="1">
                <div class="bar">
                    <div class="fill"></div>
                    <div class="fill"></div>
                    <div class="fill"></div>
                    <div class="fill"></div>
                    <div class="fill"></div>
                    <div class="fill"></div>
                    <div class="fill"></div>
                </div>
            </div>
            <label data-animate="fadeInRight" data-delay="300" data-group="1">Japanese</label>
            <div class="skill-bar skill-bar-bullets compact" data-value="75%" data-animate="fadeInRight" data-delay="1800" data-group="1">
                <div class="bar">
                    <div class="fill"></div>
                    <div class="fill"></div>
                    <div class="fill"></div>
                    <div class="fill"></div>
                    <div class="fill"></div>
                    <div class="fill"></div>
                    <div class="fill"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 padding-minus-bottom">
            <div class="sub-heading" data-animate="fadeInRight" data-group="1">Personal qualities</div>
            <ul class="list custom-bullets">
                <li data-animate="fadeInRight" data-delay="300*index" data-group="2"><span class="bullet"></span>Effective problem solver</li>
                <li data-animate="fadeInRight" data-delay="300*index" data-group="2"><span class="bullet"></span>Tactful and patient communicator</li>
                <li data-animate="fadeInRight" data-delay="300*index" data-group="2"><span class="bullet"></span>Good team player</li>
                <li data-animate="fadeInRight" data-delay="300*index" data-group="2"><span class="bullet"></span>Loyal to the company</li>
                <li data-animate="fadeInRight" data-delay="300*index" data-group="2"><span class="bullet"></span>Success motivated</li>
                <li data-animate="fadeInRight" data-delay="300*index" data-group="2"><span class="bullet"></span>Creative & hard working</li>
            </ul>
        </div>
    </div>
</div>