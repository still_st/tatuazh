jQuery(window).on('load', function(){

counter();
faq();
popup();
galery();
certificate();
workProcess();
feedbacks();
portfolio();
zoom();
slider();
offer();
windowHash();

function windowHash(){

    if (window.location.hash.indexOf("#wpcf7-f240") >= 0){
        messageAfter();
    }
}

function messageAfter(){

    elementCreateion();

    jQuery(document).on('click', '#popupMessageAfterClose', function()
    {
        jQuery('#popupMessageAfter').slideToggle(1000);
    });

    function elementCreateion(){
        jQuery('<div/>', {
            id: 'popupMessageAfter',
            class: "popup popup--MessageAfter"
        }).prependTo('#body');

        jQuery('<div/>', {
            class: 'popup--MessageAfter__inner',
        }).prependTo('#popupMessageAfter');

        jQuery('<img/>', {
            class: 'popup--MessageAfter__img',
            src: '/img/ava.jpg',
            alt: 'мастер татуажа'
        }).prependTo('.popup--MessageAfter__inner');

        jQuery('<p/>', {
            text: 'Прямо сейчас я могу быть занята, но обязательно перезвоню вам в ближайший час.'
        }).prependTo('.popup--MessageAfter__inner');

        jQuery('<div/>', {
            class: 'popup--MessageAfter__title',
            text: 'Спасибо за вашу заявку'
        }).prependTo('.popup--MessageAfter__inner');

        jQuery('<div/>', {
            id: "popupMessageAfterClose",
            class: 'popup--MessageAfter__close'
        }).prependTo('.popup--MessageAfter__inner');

        jQuery('<i/>', {
            class: 'fa fa-times'
        }).appendTo('#popupMessageAfterClose');
    }
}

function counter(){
    var counter = document.getElementById("numbers");

    if (counter != null){
        var options = {
          useEasing : true,
          useGrouping : true,
          separator : '',
          decimal : '.',
          prefix : '',
          suffix : ''
        };

    var exp = new CountUp('js-numbers__number--exp', 0, 5, 0, 5, options);
    /*var spec = new CountUp('js-numbers__number--spec', 0, 6, 0, 5, options);*/
    var ref = new CountUp('js-numbers__number--ref', 0, 1000, 0, 3, options);
    var rate = new CountUp('js-numbers__number--rate', 0, 1000, 0, 3, options);


    jQuery(document).on("scroll", function() {
        var distance = jQuery('.numbers').offset().top - jQuery(document).scrollTop();
        if (distance < 650 ){
            exp.start();
            /*spec.start();*/
            ref.start();
            rate.start();
        }
    });

    }
}

function faq(){
    jQuery(document).on('click', '.faq__q span', function()
    {
        var btn = jQuery(this);
        var question = btn.parent();
        var answer = question.next();
        question.toggleClass('faq__q--open');
        if (question.hasClass('faq__q--open')){
            btn.text('Закрыть ответ');
        }
        else{
            btn.text('Посмотреть ответ');
        }
        answer.slideToggle();
    });
}

function popup(){
    //Popup-form
    jQuery(document).on('click', '#js-popup-form__close', function()
    {
        jQuery('#js-popup-form').slideToggle();
    });

    jQuery(document).on('click', '.js-open-popup', function()
    {
        jQuery('#js-popup-form').slideToggle();
    });

    jQuery(document).on('click', '#js-popup-form', function()
    {
       if (jQuery('#js-popup-form form:hover').length == 0){
           jQuery("#js-popup-form").slideToggle();
       }
    });
}

function galery(){
    //Галерея
    var galery = document.getElementById("galery");
    if (galery != null){

        jQuery('.galery').magnificPopup({
              delegate: '.galery__img-link',
              type: 'image',
    });
    }

    //Новая Галерея
    jQuery(document).ready(function() {

        jQuery('.galery__portfolio').isotope({
          itemSelector: '.grid'
        });
    });
}

function certificate(){
    //Сертификаты
    var certificate = document.getElementById("certificate");
    if (certificate != null){
        jQuery('.certificate').magnificPopup({
              delegate: 'a',
              type: 'image',
              gallery: {
                    enabled: true
        }
    });
    }
}

function workProcess(){
    var workProcess = document.getElementById("workProcess");
    if (workProcess != null){

        jQuery('.work-process').magnificPopup({
              delegate: 'a',
              type: 'image',
              gallery: {
                    enabled: true
        }
    });
    }
}

function feedbacks(){
    //Слайдер отзывы картинки
    var owlCarouselFeed = document.getElementById("owl-carousel--feed");
    if (owlCarouselFeed != null){
        jQuery('#owl-carousel--feed').magnificPopup({
              delegate: '.galery__img-link',
              type: 'image'

    });
    }
}

function portfolio(){
    jQuery(document).on('click', '.portfolio-filter', function()
    {
       var element = jQuery(this).attr('id');
       if (element == "*"){
            jQuery('.galery__portfolio').isotope({ filter: '*' });
       }
       else{
        jQuery('.galery__portfolio').isotope({ filter: "."+element });
       }
    });
}

function zoom(){
    //Увелечение картинки меню при наведении на ссылку
    jQuery('.blog-post .sub-heading a').hover(
        function(){
            var img = this.parentNode.parentNode.querySelector('img');
            img.classList.add("scale");
        },
        function(){
            var img = this.parentNode.parentNode.querySelector('img');
            img.classList.remove("scale");
        }
    )
    jQuery('.blog-post .info a').hover(

        function(){
            console.log("done");
            var img = this.parentNode.parentNode.querySelector('img');
            img.classList.add("scale");
        },
        function(){
            var img = this.parentNode.parentNode.querySelector('img');
            img.classList.remove("scale");
        }
    )

    //вторая менюшка наведения
    jQuery('.blog-post-small .sub-heading a').hover(
        function(){
            var img = this.parentNode.parentNode.parentNode.querySelector('.image');
            img.classList.add("scale");
        },
        function(){
            var img = this.parentNode.parentNode.parentNode.querySelector('.image');
            img.classList.remove("scale");
        }
    )
    jQuery('.blog-post-small .button').hover(
        function(){
            var img = this.parentNode.parentNode.querySelector('.image');
            img.classList.add("scale");
        },
        function(){
            var img = this.parentNode.parentNode.querySelector('.image');
            img.classList.remove("scale");
        }
    )
}

function slider(){
    /*Видео отзывы*/
    jQuery('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:3
            }
        }
    })
}

function offer(){
    /*Акции*/
    jQuery(document).on('click', ".offer__front", function()
    {
        jQuery(".offer").toggleClass('offer--open');
    });
}

});


/*счётчик*/
/*
initializeClock(setDeadline(0, 0, 1, 2), false);

function initializeClock(endtime, restart) {
    var min = document.getElementById("min"),
        sec = document.getElementById("sec"),
        hour = document.getElementById("hour"),
        day = document.getElementById("day");
    var timeinterval = setInterval(function() {
        var time = getTimeRemaining(endtime);
        if (day) {
            day.innerHTML = time.days + " :";
        }
        if (hour) {
            hour.innerHTML = time.hours + " :";
        }
        if (min) {
            min.innerHTML = time.minutes + " :";
        }
        if (sec) {
            sec.innerHTML = time.seconds;
        }
        if (time.total <= 0) {
            clearInterval(timeinterval);
            if (restart) {
                initializeClock(setDeadline(10, 0, 0, 0), true);
            } else {
                if (day) {
                    day.innerHTML = "";
                }
                if (sec) {
                    sec.innerHTML = "Акция закончилась";
                }
                if (hour) {
                    hour.innerHTML = "";
                }
                if (min) {
                    min.innerHTML = "";
                }
            }
        }
    }, 1000);
}

function getTimeRemaining(endtime) {
    var time = Date.parse(endtime) - Date.parse(new Date()),
        seconds = Math.floor((time / 1000) % 60),
        minutes = Math.floor((time / 1000 / 60) % 60),
        hours = Math.floor((time / (1000 * 60 * 60)) % 24),
        days = Math.floor(time / (1000 * 60 * 60 * 24));
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    return {
        'total': time,
        'days': days,
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
    };
}

function setDeadline(seconds, minutes, hours, days) {

    var date = new Date("15 may 2017");
    var today = new Date();

    while(date < today) {
        date.setDate(date.getDate() + 15);
    }

    var season = getSeason(today);
    setPhraseByMonth(season)

    return date;
}

function getSeason(date) {
    month = date.getMonth();
    winter = 'dec,december,jan,january,feb,february,11,0,1,';
    spring = 'mar,march,apr,april,may,2,3,4,';
    summer = 'jun,june,jul,july,aug,august,5,6,7,';
    fall = 'sep,september,oct,october,nov,november,8,9,10,';
    season = 'unknown';
    if (winter.indexOf(month) != -1) {
        season = 'winter';
    } else if (spring.indexOf(month) != -1) {
        season = 'spring';
    } else if (summer.indexOf(month) != -1) {
        season = 'summer';
    } else if (fall.indexOf(month) != -1) {
        season = 'fall';
    }
    return(season);
}

function setPhraseByMonth(season){
    switch(season) {
    case 'winter':
        document.getElementById("offerPhrase").innerHTML = "зима близко";
    break

    case 'spring':
        document.getElementById("offerPhrase").innerHTML = "весне дорогу";
    break

    case 'summer':
        document.getElementById("offerPhrase").innerHTML = "питерское лето";
    break

    case 'fall':
        document.getElementById("offerPhrase").innerHTML = "осенью дешевле";
    break

    default:
        document.getElementById("offerPhrase").innerHTML = "время татуажа";
    break
    }
}
*/
/*
function changewordend(str, count, one, few, many, tens){
  if(count > 10 && count < 20)
  {
    return str + many;
  }
  var remain = count%10;
  if(remain == 1)
  {
    return str + one;
  }
  else if(remain < 5 && remain > 1)
  {
    return str + few;
  }
  else
  {
    return str + many;
  }
}
*/


/*Change Prices*/
/*
var priceTriger = false;
var firstTime = true;
var possibility = Math.random() > 0.5;

jQuery(".prices").on( "mouseleave", leavePrice);
jQuery(".hero .content").on( "mouseleave", leaveHero);
jQuery(document).on("click",'.popup-price__close', function(){
    jQuery(".popup-price").remove();
});


function leavePrice(){
    priceTriger = true;
}

function leaveHero(event){

    if ( isPopupShowing()){
        changePrices(event);
        firstTime = false;
    }

    function isPopupShowing(){
        var upLeave = event.pageY < 300;
        var noMobile = jQuery(window).width() > 650;
        if (priceTriger  && firstTime  && upLeave && noMobile ){
            return true;
        }
        return false;
    }
}


function changePrices(event){

    jQuery(".prices h1").addClass("sale-secret");
    jQuery(".prices h1").text("Цены со скидкой");

    var tablecells = jQuery(".prices table td:last-child");

    tablecells.each(function() {
        jQuery( this ).addClass( "relative" );
    });

    var item = document.createElement("div");
    var id = Math.round(Math.random() * (100 - 30) + 30);
    var msg = "<p>Мы заметили, что вы просматривали наши цены, а затем, возможно, решили покинуть наш сайт... Мы понимаем, что стоимость наших услуг может отпугнуть, но поверьте качество нашей работы того стоит. " +
    "Кроме того, <span>специально для вас</span>, мы готовы <span>сделать скидку</span>! Позвоните нам прямо сейчас по телефону <span>+7 (812)602-95-78</span> скажите фразу промокода <span> весне&nbspдорогу </span> назовите свой уникальный идентификатор <span>" + id +"</span> а мы сделаем скидку специально для вас.</p>";
    item.innerHTML =  msg;
    item.classList.add("popup-price");
    item.style.top =  event.pageY + "px";
    var leftoffset = event.pageX + "px";
    if (event.pageX + 550 > jQuery(window).width()){
        leftoffset = jQuery(window).width() - 550 + "px";
    }
    item.style.left = leftoffset;
    var closeItem = document.createElement("div");
    closeItem.classList.add("fa-times","fa", "popup-price__close");
    item.appendChild(closeItem);
    var body = document.getElementsByTagName('body')[0];
    body.appendChild(item);
    yaCounter40538475.reachGoal('luckyPrice');
}

*/